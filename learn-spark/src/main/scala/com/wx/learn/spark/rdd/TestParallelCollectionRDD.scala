package com.wx.learn.spark.rdd

import org.apache.spark.{SparkConf, SparkContext}

/**
 * @author jxlgzzw
 * @date 2020-06-14 16:22
 * @description 并行集合可以由对Driver中的集合调用parallelize方法得到
 *              sparkcontext是SparkSession对象的成员变量。
 *              这时，Driver会将集合切分成分区，并将数据分区分发到整个集群中。
 */
object TestParallelCollectionRDD {
  def main(args: Array[String]): Unit = {
    /*//创建SparkSession
    val spark = SparkSession.builder()
      .master("local[*]")
      .config("spark.reducer.maxSizeInFlight","128M")
      .appName("并行集合RDD")
      .getOrCreate()*/

    //创建SparkConf
    val conf = new SparkConf()
//      .setAppName("并行集合RDD")
      .set("spark.app.name", "并行集合RDD")
      .set("spark.master", "local[*]")

    //创建SparkContext
    val sc = SparkContext.getOrCreate(conf)

    //创建并行集合RDD
    val parallelCollectionRDD = sc.parallelize(Seq(1,2,3))

    parallelCollectionRDD.foreach(println)

    sc.stop()
  }

}
