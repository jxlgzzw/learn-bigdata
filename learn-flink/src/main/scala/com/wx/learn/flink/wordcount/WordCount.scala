package com.wx.learn.flink.wordcount

import org.apache.flink.api.scala._

/**
 * @author jxlgzzw
 * @date 2020-05-27 13:25
 * @description
 */
object WordCount {
  def main(args: Array[String]): Unit = {
    //创建一个执行环境
    val env = ExecutionEnvironment.getExecutionEnvironment

    //从文件中读取数据
    val inputPath = "G:\\code\\gitee\\learn-bigdata\\learn-flink\\src\\main\\resources\\wordcount.txt"
    val inputDataSet = env.readTextFile(inputPath)

    //处理word count:切分数据->分组->聚合
    val wordCountDataSet = inputDataSet.flatMap(_.split(" "))
      .map((_,1))
      .groupBy(0)
      .sum(1)

    //输出结果到控制台
    wordCountDataSet.print()
  }
}
