package com.wx.learn.flink.api.source

import org.apache.flink.streaming.api.scala._

/**
 * @author jxlgzzw
 * @date 2020-05-27 23:17
 * @description
 */
//传感器读取样例类
case class SensorReading(id:String,timestamp:Long,temperature:Double)

object SensorSource {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    //从自定义的集合中读取数据
    val stream = env.fromCollection(List(
      SensorReading("sensor_1", 1547718199, 35.80018327300259),
      SensorReading("sensor_6", 1547718201, 15.402984393403084),
      SensorReading("sensor_7", 1547718202, 6.720945201171228),
      SensorReading("sensor_10", 1547718205, 38.101067604893444)
    ))

    stream.print("SensorSource").setParallelism(4)

    //执行
    env.execute("SensorSource Job")

  }

}
