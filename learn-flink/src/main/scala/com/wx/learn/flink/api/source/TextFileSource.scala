package com.wx.learn.flink.api.source

import org.apache.flink.streaming.api.scala._

/**
 * @author jxlgzzw
 * @date 2020-05-27 23:27
 * @description 文件数据源
 */
object TextFileSource {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    //从文件中读取数据
    val stream = env.readTextFile("G:\\code\\gitee\\learn-bigdata\\learn-flink\\src\\main\\resources\\sensorsource.txt")

    stream.print("TextFileSource").setParallelism(4)

    //执行
    env.execute("TextFileSource Job")

  }
}
