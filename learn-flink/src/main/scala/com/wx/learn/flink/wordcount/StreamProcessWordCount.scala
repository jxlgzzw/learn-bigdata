package com.wx.learn.flink.wordcount

import org.apache.flink.streaming.api.scala._

/**
 * @author jxlgzzw
 * @date 2020-05-27 13:44
 * @description 流处理word count程序
 */
object StreamProcessWordCount {
  def main(args: Array[String]): Unit = {
    //创建流处理的执行环境
    val streamEnv = StreamExecutionEnvironment.getExecutionEnvironment

    //接受一个socket文本流
    //用linux命令nc -lk 7777发送TCP
    val dataStream = streamEnv.socketTextStream("bt1",7777)

    //对每条数据进行流处理word count
    val wordCountDataSream = dataStream.flatMap(_.split("\\s"))
      .filter(_.nonEmpty)
      .map((_,1))
      .keyBy(0)
      .sum(1)

    //打印结果到控制台,并设置并行度
    wordCountDataSream.print().setParallelism(1)

    //启动executor
    streamEnv.execute("stream process word count job")
  }
}
