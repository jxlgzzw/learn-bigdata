package com.wx.learn.flume;

import org.apache.flume.Event;
import org.apache.flume.EventDeliveryException;
import org.apache.flume.api.RpcClient;
import org.apache.flume.api.RpcClientFactory;
import org.apache.flume.event.EventBuilder;

import java.util.Properties;

//负载均衡
public class FlumeClient {
    public static void main(String[] args) throws EventDeliveryException {
        Properties prop = new Properties();

        //设置客户端类型:default,default_loadbalance,default_failover
//        prop.setProperty("client.type","default");

//        prop.setProperty("client.type","default_loadbalance");

        prop.setProperty("client.type", "default_failover");


        //设置客户端需要连接的主机,都启动相关的agent
        prop.setProperty("hosts", "h1 h2 h3");   //给主机起别名,逻辑名称,空格分隔
        //将逻辑主机名和物理地址对应
        prop.setProperty("hosts.h1", "ud2:4141");
        prop.setProperty("hosts.h2", "ud2:4142");
        prop.setProperty("hosts.h3", "ud2:4143");

        RpcClient client = RpcClientFactory.getInstance(prop);

        int num = 0;
        while (true) {
            Event event = EventBuilder.withBody(("wx" + num++).getBytes());
            client.append(event);
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
